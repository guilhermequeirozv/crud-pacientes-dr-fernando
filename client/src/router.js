import Vue from 'vue';
import Router from 'vue-router';
import ListaPacientes from './components/ListaPacientes.vue';
import Paciente from './components/Paciente.vue';
import LogIn from './components/LogIn.vue';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/login',
      name: 'LogIn',
      component: LogIn,
    },
    {
      path: '/',
      name: 'ListaPacientes',
      component: ListaPacientes,
    },
    {
      path: '/pacientes/:pid',
      name: 'Paciente',
      component: Paciente,
    },
  ],
});
// this.$route.params.pid
