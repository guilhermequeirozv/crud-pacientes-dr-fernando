#!/bin/bash
cp /home/ubuntu/project/ecosaude/scripts/ecosaude /etc/nginx/sites-available/
ln -s /etc/nginx/sites-available/ecosaude /etc/nginx/sites-enabled
systemctl restart nginx
myprocs=`ps aux |grep gunicorn |grep ecosaude | awk '{ print $2 }'`
cd /home/ubuntu/project/ecosaude/server
if ! [ -z "$myprocs" ];then
	ps aux |grep gunicorn |grep ecosaude | awk '{ print $2 }'| xargs kill -HUP	
fi

if [ -f "/home/ubuntu/project/tmp_ecosaude/ecodata.db" ]; then
	mv /home/ubuntu/project/tmp_ecosaude/ecodata.db /home/ubuntu/project/ecosaude/server/db
fi

if [ -d "/home/ubuntu/project/tmp_ecosaude/static" ]; then
	mv /home/ubuntu/project/tmp_ecosaude/static /home/ubuntu/project/ecosaude/server
else
	mkdir -p /home/ubuntu/project/ecosaude/server/static/uploaded_files
fi

sudo chown -R ubuntu:ubuntu /home/ubuntu/project/ecosaude/