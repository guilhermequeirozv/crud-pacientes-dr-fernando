import sqlite3
from sqlite3 import Error
import os


def create_database():
	try:
		dbf = open('./db/ecodata.db','w')
		dbf.close()
		conn = sqlite3.connect('./db/ecodata.db')
		cur = conn.cursor()
		sqlfile = open("./db/scripts/create_db.sql")
		sqlfile_str = sqlfile.read()
		cur.executescript(sqlfile_str)
		sqlfile.close()
		sqlfile = open("./db/scripts/movimentos.sql")
		sqlfile_str = sqlfile.read()
		cur.executescript(sqlfile_str)
		sqlfile.close()
		conn.commit
		cur.close()
		return 1
	except Exception as e:
		print(e)
		os.remove('./db/ecodata.db')
		return 0

def cdu_dbobject(dbobject,sql):
    try:
        if (os.path.isfile('./db/ecodata.db')) is False:
        	create_database()
        conn = sqlite3.connect('./db/ecodata.db')
        cur = conn.cursor()
        cur.execute(sql, dbobject)
        conn.commit()
        cur.close()
        return 1
    except Exception as e:
        print(e)
        return 0


def s_dbobject(sql, filter):
    try:
        if (os.path.isfile('./db/ecodata.db')) is False:
        	if create_database() == 0:
        		return None
        conn = sqlite3.connect('./db/ecodata.db')
        cur = conn.cursor()
        cur.execute(sql, filter)
        dbobjects = []
        columns = list(map(lambda x: x[0], cur.description))
        for row in cur:
            dbobject = {}
            for i in range(len(row)):
                dbobject[columns[i]] = row[i]
            dbobjects.append(dbobject)
        return dbobjects
    except Exception as e:
        print(e)
        return None

def update_paciente(paciente):
    sql = ''' UPDATE PACIENTE SET nome = :nome, estatura = :estatura, nascimento = :nascimento,
              inicio = :inicio, queixa = :queixa, genero = :genero WHERE id = :id '''
    return cdu_dbobject(paciente,sql)

def delete_paciente(paciente):
    sql = ''' DELETE FROM PACIENTE WHERE id = :id '''
    return cdu_dbobject(paciente,sql)

def select_pacientes():
    sql = ''' SELECT id, nome, estatura, peso, nascimento, inicio, queixa, genero 
              FROM PACIENTE ORDER BY nome'''
    return s_dbobject(sql,{})

def autenticar(usuario):
    sql = ''' SELECT * from usuarios_ecodata where user = upper(:user) and password = :password '''
    return s_dbobject(sql,usuario)

def select_paciente(paciente):
    sql = ''' SELECT id, nome, estatura, peso, nascimento, inicio, queixa, genero 
              FROM PACIENTE WHERE id = :id '''
    result = s_dbobject(sql, paciente)
    paciente = None if result is None else result[0]
    return paciente
    
def insert_paciente(paciente):
    sql = ''' INSERT INTO paciente(nome,estatura,peso,nascimento,inicio,queixa,genero)
              VALUES(:nome,:estatura,:peso,:nascimento,:inicio,:queixa,:genero) '''
    return cdu_dbobject(paciente,sql)

def insert_leitura(leitura):
    sql = ''' INSERT INTO 
              leitura(paciente_id,tempo,movimento_id,pico,media,
              dominante,data,path,dominado_id) VALUES
              (:paciente_id,:tempo,:movimento_id,:pico,:media,:dominante,
              :data,:path,:dominado_id)
          '''
    return cdu_dbobject(leitura,sql)

def update_dominx(leitura,index):
    sql = ''
    if index == 1:
        sql = ''' UPDATE LEITURA SET dominante=0,dominado_id = 0 WHERE id = 
            (SELECT dominado_id FROM leitura WHERE id = :id) '''
    else:
        sql = ''' UPDATE LEITURA SET dominante=0,dominado_id = :id WHERE id = :dominado_id '''
    return cdu_dbobject(leitura,sql)

def getPreviousDomxInfo(leitura):
    sql = ''' SELECT dominante,dominado_id FROM LEITURA WHERE id = :id '''
    return s_dbobject(sql,leitura)

def update_leitura(leitura):
    sql = ''' UPDATE leitura SET tempo = :tempo, movimento_id = :movimento_id, pico = :pico,
              media = :media, dominante = :dominante, data = :data,
              dominado_id = :dominado_id
          '''
    if leitura['path'] is not None:
        sql = sql + ''' , path = :path '''
    sql = sql + ''' WHERE id = :id and paciente_id = :paciente_id '''
    if int(leitura['dominante']) == 1:
        update_dominx(leitura,1)
        cdu_dbobject(leitura,sql)
        update_dominx(leitura,0)  
    else:
        dominante = getPreviousDomxInfo(leitura)
        if dominante[0]['dominante'] == int(leitura['dominante']):
            #logica para deixar como esta
            leitura['dominado_id'] = dominante[0]['dominado_id']
            cdu_dbobject(leitura,sql)  
        else:
            #logica para sair de 1 e ir pra 0 
            update_dominx(leitura,1)
            leitura['dominado_id'] = '0'
            cdu_dbobject(leitura,sql) 
    return 1

def delete_leitura(leitura):
    sql = ''' DELETE FROM LEITURA WHERE id = :id AND paciente_id = :paciente_id '''
    return cdu_dbobject(leitura,sql)

def select_movimentos():
    sql = ''' SELECT id as value, id as text FROM MOVIMENTO '''
    return s_dbobject(sql, {})

def select_leituras(paciente):
    sql = ''' SELECT l1.id,l1.paciente_id,l1.tempo,l1.movimento_id,l1.pico,l1.media,
            l1.dominante,l1.data,l1.dominado_id,l1.path,
            case 
            when l2.id is null then 0
            when l1.dominante == 1 then round(((l1.media-l2.media)/(0.5*(l1.media+l2.media)))*100,2)
            when l2.dominante == 1 then round(((l2.media-l1.media)/(0.5*(l2.media+l1.media)))*100,2)
            end as assimetria,vp.forca_n as referencia
            FROM LEITURA l1 
			left JOIN paciente p on p.id = l1.paciente_id
			left JOIN valores_referencia vp on 
			vp.genero = p.genero and vp.dominante = l1.dominante
			and cast(strftime('%Y.%m%d', 'now') - strftime('%Y.%m%d', p.nascimento) as int) 
			between idade_min and idade_max AND
			l1.movimento_id like vp.membro||' '||vp.nome||'%'
			left JOIN leitura l2 on
            l2.paciente_id = l1.paciente_id 
            and l2.id = l1.dominado_id and l2.dominado_id = l1.id 
            where l1.paciente_id = :paciente_id'''
    return s_dbobject(sql, paciente)

def select_maxvalue(paciente):
    sql = ''' select max(elem) as maxvalue from (
              select media as elem from leitura where paciente_id = :paciente_id union 
              select pico as elem from leitura where paciente_id = :paciente_id union
              SELECT 
              case 
              when l2.id is null then 0
              when l1.dominante == 1 then abs(round(((l1.media-l2.media)/(0.5*(l1.media+l2.media)))*100 ,2))
              when l2.dominante == 1 then abs(round(((l2.media-l1.media)/(0.5*(l2.media+l1.media)))*100 ,2))
              end as assimetria
              FROM LEITURA l1 left JOIN leitura l2 on
              l2.paciente_id = l1.paciente_id 
              and l2.id = l1.dominado_id and l2.dominado_id = l1.id 
              where l1.paciente_id = :paciente_id
              ) '''
    result = s_dbobject(sql, paciente)
    max_value = 0 if result is None else result[0]
    return max_value