DROP TABLE IF EXISTS LEITURA;
DROP TABLE IF EXISTS MOVIMENTO;
DROP TABLE IF EXISTS PACIENTE;

CREATE TABLE "movimento" (
	"nome"	TEXT,
	"ilustracao"	TEXT,
	"membro"	TEXT,
	"lado"	TEXT,
	"id"	TEXT,
	PRIMARY KEY("id")
);

CREATE TABLE "paciente" (
	"id"	INTEGER,
	"nome"	TEXT,
	"estatura"	INTEGER,
	"peso"	INTEGER,
	"nascimento"	TEXT,
	"inicio"	TEXT,
	"queixa"	TEXT,
	"genero"	TEXT,
	PRIMARY KEY("id" AUTOINCREMENT)
);

CREATE TABLE "leitura" (
	"id"	INTEGER,
	"paciente_id"	INTEGER,
	"tempo"	INTEGER,
	"movimento_id"	TEXT,
	"pico"	NUMERIC,
	"media"	NUMERIC,
	"amplitude"	NUMERIC,
	"assimetria"	NUMERIC,
	"data"	TEXT,
	"dominante"	INTEGER,
	"path"	TEXT,
	"dominado_id" integer default 0,
	FOREIGN KEY("paciente_id") REFERENCES "paciente"("id") on delete cascade,
	PRIMARY KEY("id"),
	FOREIGN KEY("movimento_id") REFERENCES "movimento"("id")
);


create trigger if not exists create_dominx_pair_ins_1 after INSERT
on leitura when new.dominante == 1
BEGIN
	update leitura set dominado_id = new.id where id = new.dominado_id;
END;


create trigger if not exists remove_dominx_pair_ins_1 before delete
on leitura
BEGIN
	update leitura set dominado_id = 0,dominante=0 where id = old.dominado_id;
END;

CREATE UNIQUE INDEX "leitura_idx" ON "leitura" (
	"id",
	"paciente_id"
);

CREATE UNIQUE INDEX "paciente_idx" ON "paciente" (
	"id"
);

