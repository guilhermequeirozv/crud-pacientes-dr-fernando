import db
import uuid
from flask import Flask, session, jsonify, request
from flask_cors import CORS, cross_origin
import os
from flask_session import Session
import hashlib 

# configuration
DEBUG = True

# instantiate the app
app = Flask(__name__)
app.config.from_object(__name__)
app.secret_key = os.urandom(24)
app.config.update(SESSION_COOKIE_SAMESITE="None", SESSION_COOKIE_SECURE=True)
#enable SESSION
SECRET_KEY = "changeme"
SESSION_TYPE = 'filesystem'
app.config.from_object(__name__)
Session(app)
# enable CORS
CORS(app, resources={r'/*': {'origins': '*'}})


#@app.route('/logout', methods=["GET"])
#def logout():
#    if 'sessionKey' in session:
#        del session['sessionKey']
#        return jsonify(1)
#    return jsonify(0)

@app.route('/login', methods=['POST'])
@cross_origin(supports_credentials=True)
def login():
    if 'sessionKey' in session:
        del session['sessionKey']
    post_data = request.get_json()
    if 'password' in post_data and 'user' in post_data:
            usuario = db.autenticar({
                'user': post_data.get('user'), 
                'password': post_data.get('password')
            })
            if len(usuario) > 0:
                session['sessionKey'] = str(uuid.uuid4().hex)
                return jsonify(session['sessionKey'])
            else:
                return jsonify(None)
    else:
        return jsonify(None)
    return jsonify(None)

@app.route('/movimentos', methods=['GET'])
@cross_origin(supports_credentials=True)
def all_movimentos():
    response_object = {'status': 'success'}
    if request.method == 'GET':
        response_object['movimentos'] = db.select_movimentos()
    return jsonify(response_object)

@app.route('/pacientes/<paciente_id>/max', methods=['GET'])
@cross_origin(supports_credentials=True)
def max_value(paciente_id):
    response_object = {'status': 'success'}
    if request.method == 'GET':
        response_object['max_value'] = db.select_maxvalue({'paciente_id':paciente_id})
    return jsonify(response_object)

@app.route('/pacientes', methods=['GET', 'POST'])
@cross_origin(supports_credentials=True)
def all_pacientes():
    response_object = {'status': 'success'}
    if request.method == 'POST':
        post_data = request.get_json()
        paciente = {
            'nome': post_data.get('nome'),
            'estatura': post_data.get('estatura'),
            'peso': post_data.get('peso'),
            'nascimento': post_data.get('nascimento'),
            'inicio': post_data.get('inicio'),
            'queixa': post_data.get('queixa'),
            'genero': post_data.get('genero'),
        }
        db.insert_paciente(paciente)
        response_object['message'] = 'Paciente adicionado!'
    if request.method == 'GET':
        response_object['pacientes'] = db.select_pacientes()
    return jsonify(response_object)


@app.route('/pacientes/<paciente_id>', methods=['GET', 'PUT', 'DELETE'])
@cross_origin(supports_credentials=True)
def single_paciente(paciente_id):
    response_object = {'status': 'success'}
    if request.method == 'PUT':
        post_data = request.get_json()
        paciente = {
            'id': post_data.get('id'),
            'nome': post_data.get('nome'),
            'estatura': post_data.get('estatura'),
            'peso': post_data.get('peso'),
            'nascimento': post_data.get('nascimento'),
            'inicio': post_data.get('inicio'),
            'queixa': post_data.get('queixa'),
            'genero': post_data.get('genero')
        }
        db.update_paciente(paciente)
        response_object['message'] = 'Paciente Atualizado!'
    if request.method == 'DELETE':
        db.delete_paciente({'id':paciente_id})
        response_object['message'] = 'Paciente removido!'
    if request.method == 'GET':
        paciente = db.select_paciente({'id':paciente_id})
        if paciente:
            paciente['leituras'] = db.select_leituras({'paciente_id':paciente_id})
        response_object['paciente'] = paciente
    return jsonify(response_object)


@app.route('/pacientes/<paciente_id>/leituras', methods=['GET', 'POST'])
@cross_origin(supports_credentials=True)
def all_leituras(paciente_id):
    response_object = {'status': 'success'}
    if request.method == 'POST':
        post_data = request.form
        file_dest = None
        if request.files is not None and request.files.get('file') is not None:
            img_id = uuid.uuid4().hex
            file_name = img_id+request.files['file'].filename
            request.files['file'].save('./static/uploaded_files/'+file_name)
            file_dest = request.url_root+'static/uploaded_files/'+file_name
        leitura = {
            'paciente_id': paciente_id,
            'tempo': post_data.get('tempo'),
            'movimento_id': post_data.get('movimento_id'),
            'pico': post_data.get('pico'),
            'media': post_data.get('media'),
            'dominante': post_data.get('dominante'),
            'dominado_id': post_data.get('dominado_id'),
            'data': post_data.get('data'),
            'path': file_dest
        }
        db.insert_leitura(leitura)
    if request.method == 'GET':
        response_object['leituras'] = db.select_leituras({'paciente_id':paciente_id})
    return jsonify(response_object)


@app.route('/pacientes/<paciente_id>/leituras/<leitura_id>', methods=['PUT', 'DELETE'])
@cross_origin(supports_credentials=True)
def single_leitura(paciente_id, leitura_id):
    response_object = {'status': 'success'}
    if request.method == 'PUT':
        post_data = request.form
        file_dest = None
        if request.files is not None and request.files.get('file') is not None:
            img_id = uuid.uuid4().hex
            file_name = img_id+request.files['file'].filename
            request.files['file'].save('./static/uploaded_files/'+file_name)
            file_dest = request.url_root+'static/uploaded_files/'+file_name
        leitura = {
            'id': leitura_id,
            'paciente_id': paciente_id,
            'tempo': post_data.get('tempo'),
            'movimento_id': post_data.get('movimento_id'),
            'pico': post_data.get('pico'),
            'media': post_data.get('media'),
            'dominante': post_data.get('dominante'),
            'dominado_id': post_data.get('dominado_id'),
            'data': post_data.get('data'),
            'path': file_dest
        }
        db.update_leitura(leitura)
        response_object['message'] = 'Leitura Atualizado!'
    if request.method == 'DELETE':
        db.delete_leitura({'paciente_id':paciente_id, 'id':leitura_id})
        response_object['message'] = 'Leitura removido!'
    return jsonify(response_object)



if __name__ == '__main__':    
    app.run()